import Vue from 'vue'
import Vuex from 'vuex'
import translation from './modules/translations'
Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        translation,
    }
})
