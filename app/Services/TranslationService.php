<?php

namespace App\Services;

use App\Http\Traits\Countries;
use App\Http\Traits\SupportedLanguages;
use App\Services\TranslationServiceInterface;
use JoggApp\GoogleTranslate\GoogleTranslateClient;

class TranslationService implements TranslationServiceInterface
{
    use SupportedLanguages, Countries;
    private $translateClient;

    public function __construct(GoogleTranslateClient $client)
    {
        $this->translateClient = $client;
    }

    public function fetchCountries(): array
    {
        return $this->countries();
    }

    public function translate($request, $to, $format): \Illuminate\Http\JsonResponse
    {
        $data = [
            'success' => false,
        ];
        $to = trim(strtolower($request['iso']));
        $translateTo = $to ?? config('googletranslate.default_target_translation');
        $input = $request['text'];
        $translateTo = $this->sanitizeLanguageCode($translateTo);

        $response = $this
            ->translateClient
            ->translate($input, $translateTo, $format);

        if($response){
            $data = [
                'success' => true,
                'source_text' => $input,
                'source_language_code' => $response['source'],
                'translated_text' => $response['text'],
                'translated_language_code' => $translateTo
            ];
        }

        return response()->json($data);
    }

    public function sanitizeLanguageCode($languageCode)
    {
        if ($languageCode === 'zh-tw') {
            $languageCode = 'zh-TW';
        }

        if (in_array($languageCode, $this->languages())) {
            return $languageCode;
        }

        throw new \Exception(
            "Invalid or unsupported ISO 639-1 language code -{$languageCode}-"
        );
    }
}
