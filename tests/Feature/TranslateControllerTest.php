<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JoggApp\GoogleTranslate\GoogleTranslate;
use Tests\TestCase;
use JoggApp\GoogleTranslate\GoogleTranslateClient;

class TranslateControllerTest extends TestCase
{
    private $translateClient;

    private $translate;

    public function setUp(): void
    {
        parent::setUp();

        $this->translateClient = \Mockery::mock(GoogleTranslateClient::class);

        $this->translate = new GoogleTranslate($this->translateClient);
    }

    /** @test */
    public function it_sanitizes_the_language_codes()
    {
        $response = $this->translate->sanitizeLanguageCode('en');
        $this->assertEquals('en', $response);

        $response = $this->translate->sanitizeLanguageCode('en');
        $this->assertEquals('en', $response);

        $response = $this->translate->sanitizeLanguageCode('EN');
        $this->assertEquals('en', $response);

        // 'zh-TW' is the only language code defined by google that includes uppercase letters
        $response = $this->translate->sanitizeLanguageCode('zh-TW');
        $this->assertEquals('zh-TW', $response);

        $this->expectExceptionMessage(
            'Invalid or unsupported ISO 639-1 language code -xx-'
        );
        $this->translate->sanitizeLanguageCode('xx');
    }
}
