<?php

use Illuminate\Support\Facades\Route;

Route::get('{path}', 'HomeController')->where('path', '(.*)');
