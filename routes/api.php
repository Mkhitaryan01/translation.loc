<?php

use Illuminate\Support\Facades\Route;

Route::post('translation', 'TranslateController@translate');
Route::get('countries', 'TranslateController@getCountries');
