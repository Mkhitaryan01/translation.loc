# Google translate

> Google translate


## Features

- Laravel 8
- Vue + VueRouter + Vuex
- Pages with dynamic import and custom layouts
- Home
- Bootstrap 4

## Usage

#### Development

- git clone https://Mkhitaryan01@bitbucket.org/Mkhitaryan01/translation.loc.git
- composer install
- cp .env.example .env
- npm install
- npm run dev

```bash
# Build and watch
npm run watch
```
