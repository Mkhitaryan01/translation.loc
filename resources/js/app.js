import App from './components/App'
import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './router/routes.js'
import store from './store/index'
import { BootstrapVue } from 'bootstrap-vue'

Vue.use(BootstrapVue)
Vue.use(VueRouter)

new Vue({
    el: '#app',
    router: routes,
    store,
    BootstrapVue,
    ...App
})
