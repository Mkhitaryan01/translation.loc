import * as axios from 'axios'

export default {
    state: () => ({
        translations: {},
        countries: {}
    }),
    getters: {
        GET_TRANSLATION(state) {
            return state.translations
        },
        GET_COUNTRIES(state) {
            return state.countries
        }
    },
    mutations: {
        SET_TRANSLATION (state, translations) {
            state.translations = translations
        },
        SET_COUNTRIES (state, countries) {
            state.countries = countries
        }
    },
    actions: {
        LOAD_TRANSLATION({commit}, data) {
            return axios.post('/api/translation', data)
                .then(response => {
                    commit('SET_TRANSLATION', response.data)
                }).catch((error) => {
                    console.log(error.response.data.message);
                })
        },
        LOAD_COUNTRIES({commit}) {
            return axios.get('/api/countries')
                .then(response => {
                    commit('SET_COUNTRIES', response.data)
                }).catch((error) => {
                    console.log(error)})
        }
    }
}
