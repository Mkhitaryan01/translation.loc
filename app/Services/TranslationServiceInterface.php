<?php

namespace App\Services;

interface TranslationServiceInterface
{
    public function fetchCountries();
    public function translate($request, $to, $format);
    public function sanitizeLanguageCode($languageCode);
}
