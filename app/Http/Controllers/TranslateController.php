<?php

namespace App\Http\Controllers;

use App\Http\Requests\TranslationRequest;
use App\Services\TranslationServiceInterface;

class TranslateController extends Controller
{
    private $translateService;

    public function __construct(TranslationServiceInterface $translateService)
    {
        $this->translateService = $translateService;
    }

    public function getCountries(): array
    {
        return $this->translateService->fetchCountries();
    }

    public function translate(TranslationRequest $request, $to = null, $format = 'text'): \Illuminate\Http\JsonResponse
    {
        return $this->translateService->translate($request->all(), $to = null, $format = 'text');
    }
}
